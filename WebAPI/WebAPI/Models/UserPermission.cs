﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class UserPermission
    {
        public int UserPermissionId { get; set; }
        public int UserId { get; set; }
        public int PermissionId { get; set; }
        public int? StoreId { get; set; }
    }
}
