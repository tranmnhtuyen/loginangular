﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public partial class SupplierContractQuantityCustomsDeclarationTotal
    {
        public string Blnumber { get; set; }
        public string InvoiceNumber { get; set; }
        public double? ExchangeRate { get; set; }
        public int? QuantityTotal { get; set; }
        public double? CurrencyTotal { get; set; }
        public double? Vndtotal { get; set; }
        public double? ImportTaxMoneyTotal { get; set; }
        public double? Vattotal { get; set; }
        public double? TaxMoneyTotal { get; set; }
    }
}
